namespace CommandBus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using global::CommandBus.Contracts;
    using AppFunc = System.Func<CommandBusContext, System.Func<System.Threading.Tasks.Task>, System.Threading.Tasks.Task>;

    public abstract class CommandBusPipeline
    {
        private readonly List<object> middlewareList;

        protected CommandBusPipeline(object[] middlewareList)
        {
            if (middlewareList == null)
            {
                throw new ArgumentNullException("middlewareList");
            }
            this.middlewareList = new List<object>(middlewareList);
        }

        public async Task Send(CommandBusContext context)
        {
            var middleware = this.Resolve(context.Command);
            for (var i = this.middlewareList.Count - 1; i > -1; --i)
            {
                var next = middleware;
                var middlewareType = this.middlewareList[i] as Type;
                if (middlewareType != null)
                {
                    middleware = this.Resolve(middlewareType);
                    middleware.Next = next;
                }
                else
                {
                    var appFunc = this.middlewareList[i] as AppFunc;
                    if (appFunc != null)
                    {
                        middleware = new AppFuncCommandBusMiddleware(appFunc);
                        middleware.Next = next;
                    }
                }
            }
            await middleware.Send(context).ConfigureAwait(false);
        }

        protected abstract CommandBusMiddleware Resolve(Command command);

        protected abstract CommandBusMiddleware Resolve(Type middlewareType);

        internal sealed class AppFuncCommandBusMiddleware : CommandBusMiddleware
        {
            private readonly AppFunc execute;

            public AppFuncCommandBusMiddleware(AppFunc execute)
            {
                this.execute = execute;
            }

            public override Task Send(CommandBusContext context)
            {
                return this.execute(context, () => this.Next.Send(context));
            }
        }
    }
}