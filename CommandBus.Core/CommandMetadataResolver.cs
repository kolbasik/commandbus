﻿namespace CommandBus.Core
{
    using System;
    using System.Collections.Concurrent;

    using global::CommandBus.Contracts;

    public sealed class CommandMetadataResolver
    {
        private readonly ConcurrentDictionary<Type, CommandMetadata> serviceProviderMetadataMap;

        public CommandMetadataResolver()
        {
            this.serviceProviderMetadataMap = new ConcurrentDictionary<Type, CommandMetadata>();
        }

        public CommandMetadata ResolveCommandHandlerMetadata(Type commandType)
        {
            return this.serviceProviderMetadataMap.GetOrAdd(commandType, GetServiceProviderMetadata);
        }

        private static CommandMetadata GetServiceProviderMetadata(Type commandType)
        {
            var meta = new CommandMetadata();
            meta.CommandType = commandType;
            meta.CommandResultType = GetCommandResultType(commandType);
            meta.CommandHandlerType = typeof(ICommandHandler<>).MakeGenericType(commandType);
            meta.CommandHandleMethod = meta.CommandHandlerType.GetMethod("Handle", new[] { commandType });
            return meta;
        }

        private static Type GetCommandResultType(Type commandType)
        {
            var commandResultType = typeof(CommandResult);

            var commandBaseType = commandType.BaseType;
            var commandForType = (commandBaseType != null && commandBaseType.IsGenericType) ? commandBaseType.GetGenericTypeDefinition() : null;
            if (commandForType != null && commandForType == typeof(CommandFor<>))
            {
                commandResultType = commandForType.GetGenericArguments()[0];
            }

            return commandResultType;
        }
    }
}