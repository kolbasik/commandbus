﻿namespace CommandBus.Core
{
    using System;
    using System.Collections.Generic;

    using global::CommandBus.Contracts;

    public class CommandBusContext
    {
        public CommandBusContext(Command command)
        {
            this.Data = new Dictionary<string, object>();
            this.Command = command;
        }

        public Dictionary<string, object> Data { get; private set; }

        public Exception Exception { get; set; }

        public Command Command { get; private set; }

        public CommandResult CommandResult { get; set; }
    }
}