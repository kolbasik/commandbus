namespace CommandBus.Core
{
    using System.Threading.Tasks;

    public abstract class CommandBusMiddleware
    {
        public CommandBusMiddleware Next { get; set; }

        public abstract Task Send(CommandBusContext context);
    }
}