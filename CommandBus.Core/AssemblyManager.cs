namespace CommandBus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class AssemblyManager
    {
        public static IEnumerable<Assembly> GetLoadedAssembliesFromApplicationBaseDirectory()
        {
            var domain = AppDomain.CurrentDomain;
            return from assembly in domain.GetAssemblies()
                   where
                       assembly.ManifestModule.Name != "<In Memory Module>"
                       && assembly.ManifestModule.ScopeName != "vshost32.exe"
                       && !assembly.FullName.StartsWith("mscorlib")
                       && !assembly.FullName.StartsWith("SMDiagnostics")
                       && !assembly.FullName.StartsWith("System")
                       && !assembly.FullName.StartsWith("Microsoft")
                       && !assembly.FullName.StartsWith("Newtonsoft.Json")
                       && !string.IsNullOrEmpty(assembly.Location)
                       && assembly.Location.StartsWith(domain.RelativeSearchPath, StringComparison.OrdinalIgnoreCase)
                   select assembly;
        }
    }
}