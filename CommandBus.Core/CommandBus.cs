﻿namespace CommandBus.Core
{
    using System;
    using System.Threading.Tasks;

    using global::CommandBus.Contracts;

    public class CommandBus : ICommandBus
    {
        private readonly CommandBusPipeline pipeline;

        public CommandBus(CommandBusPipeline pipeline)
        {
            if (pipeline == null)
            {
                throw new ArgumentNullException("pipeline");
            }
            this.pipeline = pipeline;
        }

        public async Task<CommandResult> Submit(Command command)
        {
            if (command == null)
            {
                throw new ArgumentNullException("command");
            }

            var context = new CommandBusContext(command);
            await this.pipeline.Send(context).ConfigureAwait(false);
            if (context.Exception != null)
            {
                throw context.Exception;
            }
            return context.CommandResult;
        }
    }
}