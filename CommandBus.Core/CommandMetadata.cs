namespace CommandBus.Core
{
    using System;
    using System.Reflection;

    public sealed class CommandMetadata
    {
        public Type CommandType { get; set; }

        public Type CommandResultType { get; set; }

        public Type CommandHandlerType { get; set; }

        public MethodInfo CommandHandleMethod { get; set; }
    }
}