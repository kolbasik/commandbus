namespace CommandBus.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    using global::CommandBus.Contracts;

    public abstract class CommandBusBuilder<TCommandBus>
        where TCommandBus : ICommandBus
    {
        protected List<object> MiddlewareList { get; private set; }

        protected List<CommandBusMetadata> MetadataList { get; private set; }

        public CommandBusBuilder()
        {
            this.MiddlewareList = new List<object>();
            this.MetadataList = new List<CommandBusMetadata>();
        }

        public abstract TCommandBus BuildCommandBus();

        public void Use<TMiddleware>() where TMiddleware : CommandBusMiddleware
        {
            this.Use(typeof(TMiddleware));
        }

        public void Use(Type middlewareType)
        {
            this.MiddlewareList.Add(middlewareType);
        }

        public void Use(Action<CommandBusContext, Action> execute)
        {
            this.MiddlewareList.Add(execute);
        }

        public void RegisterCommandHandlersFromApplicationBaseDirectory()
        {
            var assemblies = AssemblyManager.GetLoadedAssembliesFromApplicationBaseDirectory().ToArray();
            this.RegisterCommandHandlersFrom(assemblies);
        }

        public void RegisterCommandHandlersFrom(params Assembly[] assemblies)
        {
            var commandHandlerType = typeof(ICommandHandler<>);
            foreach (var assembly in assemblies)
            {
                foreach (var exportedType in assembly.ExportedTypes)
                {
                    foreach (var exportedInterface in exportedType.GetInterfaces())
                    {
                        if (exportedInterface.IsGenericType
                            && exportedInterface.GetGenericTypeDefinition() == commandHandlerType)
                        {
                            this.Register(exportedType, exportedInterface, exportedInterface.GetGenericArguments()[0]);
                        }
                    }
                }
            }
        }

        public void Register<TCommandHandler, TCommand>()
            where TCommandHandler : ICommandHandler<TCommand>
            where TCommand : Command
        {
            this.Register(typeof(TCommandHandler), typeof(ICommandHandler<TCommand>), typeof(TCommand));
        }

        public void Register(Type commandHandlerType, Type commanHandlerInterface, Type commandType)
        {
            var metadata = new CommandBusMetadata
                               {
                                   CommandHandlerType = commandHandlerType,
                                   CommandHandlerInterface = commanHandlerInterface,
                                   CommandType = commandType
                               };
            this.MetadataList.Add(metadata);
        }
    }
}