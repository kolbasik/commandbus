namespace CommandBus.Core
{
    using System;

    public sealed class CommandBusMetadata
    {
        public Type CommandHandlerType { get; set; }

        public Type CommandHandlerInterface { get; set; }

        public Type CommandType { get; set; }
    }
}