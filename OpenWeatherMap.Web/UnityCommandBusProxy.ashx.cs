﻿namespace OpenWeatherMap.Web
{
    using CommandBus.Contracts;
    using CommandBus.Http.Server;
    using CommandBus.Unity;

    using OpenWeatherMap.BL;

    /// <summary>
    ///     Summary description for CommandBus
    /// </summary>
    public class UnityHttpCommandBusHandler : HttpCommandBusHandler
    {
        private readonly static ICommandBus SharedCommandBus;

        static UnityHttpCommandBusHandler()
        {
            var unityCommandBusBuilder = new UnityCommandBusBuilder();
            unityCommandBusBuilder.RegisterCommandHandlersFrom(typeof(OpenWeatherMapStartup).Assembly);
            SharedCommandBus = unityCommandBusBuilder.BuildCommandBus();
        }

        public override ICommandBus CommandBus
        {
            get
            {
                return SharedCommandBus;
            }
        }
    }
}