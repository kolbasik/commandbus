﻿namespace OpenWeatherMap.Web
{
    using CommandBus.Contracts;
    using CommandBus.Http.Server;
    using CommandBus.ServiceProvider;

    using OpenWeatherMap.BL;

    /// <summary>
    ///     Summary description for CommandBus
    /// </summary>
    public class ServiceProviderHttpCommandBusHandler : HttpCommandBusHandler
    {
        private readonly static ICommandBus SharedCommandBus;

        static ServiceProviderHttpCommandBusHandler()
        {
            var serviceProviderCommandBusBuilder = new ServiceProviderCommandBusBuilder();
            serviceProviderCommandBusBuilder.RegisterCommandHandlersFrom(typeof(OpenWeatherMapStartup).Assembly);
            SharedCommandBus = serviceProviderCommandBusBuilder.BuildCommandBus();
        }

        public override ICommandBus CommandBus
        {
            get
            {
                return SharedCommandBus;
            }
        }
    }
}