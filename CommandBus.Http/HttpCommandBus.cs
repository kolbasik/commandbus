﻿namespace CommandBus.Http
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using CommandBus.Contracts;

    using Newtonsoft.Json;

    public class HttpCommandBus : ICommandBus // NOTE: ServiceClient in ServiceStack
    {
        private readonly Uri remoteServerUri;

        private readonly HttpClient client;

        public HttpCommandBus(Uri remoteServerUri)
        {
            this.remoteServerUri = remoteServerUri;
            this.client = new HttpClient(new HttpRetryHandler(new HttpClientHandler()));
        }

        public async Task<CommandResult> Submit(Command command)
        {
            CommandResult commandResult = null;
            var upstream = JsonConvert.SerializeObject(command);

            using(var requestMessage = new HttpRequestMessage(HttpMethod.Post, this.remoteServerUri))
            {
                requestMessage.Headers.Connection.Add("Close"); // NOTE: Windows 7 has an critical issue with limited opened tcp connections. cmd.exe -k "netstat -n"
                requestMessage.Headers.Add(HttpCommandBusConstants.Headers.CommandType, command.GetType().AssemblyQualifiedName);
                requestMessage.Content = new StringContent(upstream, Encoding.UTF8, "application/json");

                try
                {
                    using (var responseMessage = await this.client.SendAsync(requestMessage).ConfigureAwait(false))
                    {
                        var downstream = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);

                        if (responseMessage.StatusCode == HttpStatusCode.OK
                            || responseMessage.StatusCode == HttpStatusCode.BadRequest)
                        {
                            IEnumerable<string> commandResultTypes;
                            if (responseMessage.Headers.TryGetValues(
                                HttpCommandBusConstants.Headers.CommandResultType,
                                out commandResultTypes))
                            {
                                var commandResultType = Type.GetType(commandResultTypes.Single());
                                commandResult = (CommandResult)JsonConvert.DeserializeObject(downstream, commandResultType);
                            }

                            var fault = commandResult as FaultCommandResult;
                            if (fault != null)
                            {
                                throw new FaultCommandResultException(fault.Message, fault.Details);
                            }
                        }
                        else
                        {
                            throw new FaultCommandResultException(responseMessage.ReasonPhrase, downstream);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new FaultCommandResultException(ex.Message, ex);
                }
            }
            return commandResult;
        }
    }
}