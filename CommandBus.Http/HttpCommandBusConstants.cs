﻿namespace CommandBus.Http
{
    public static class HttpCommandBusConstants
    {
        public static class Headers
        {
            public const string CommandType = @"X-SB-COMMAND_TYPE";
            public const string CommandResultType = @"X-SB-COMMAND_RESULT_TYPE";
        }
    }
}