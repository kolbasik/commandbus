﻿namespace CommandBus.Http
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Http;

    internal static class HttpExceptionHandler
    {
        public static readonly List<WebExceptionStatus> WebExceptionStatuses;

        static HttpExceptionHandler()
        {
            WebExceptionStatuses = new List<WebExceptionStatus>
                                       {
                                           WebExceptionStatus.Timeout,
                                           WebExceptionStatus.SendFailure,
                                           WebExceptionStatus.ReceiveFailure,
                                           WebExceptionStatus.KeepAliveFailure,
                                           WebExceptionStatus.ConnectFailure,
                                           WebExceptionStatus.ConnectionClosed,
                                           WebExceptionStatus.RequestCanceled
                                       };
        }

        public static bool IsTransient(Exception ex)
        {
            var aggregateException = ex as AggregateException;
            if (aggregateException != null)
            {
                ex = aggregateException.InnerException;
            }

            var webException = ex.InnerException as WebException;
            if (webException != null)
            {
                Debug.WriteLine("WebException: {0} - {1}", webException.Status, webException.Message);
                return WebExceptionStatuses.Contains(webException.Status);
            }

            var httpRequestException = ex as HttpRequestException;
            if (httpRequestException != null)
            {
                Debug.WriteLine("HttpRequestException: {0}", httpRequestException.Message);
                if (httpRequestException.Message.Contains("404 (Not Found)"))
                {
                    return true;
                }
            }

            return false;
        }
    }
}