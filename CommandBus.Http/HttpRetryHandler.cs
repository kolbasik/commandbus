namespace CommandBus.Http
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;

    internal sealed class HttpRetryHandler : DelegatingHandler
    {
        private const int MaxRetryCount = 5;
        private const int IncrementTime = 100;

        public HttpRetryHandler(HttpMessageHandler innerHandler)
            : base(innerHandler)
        {
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var attempt = 0;
            var delayTime = 0;

            HttpResponseMessage response;
            while (true)
            {
                try
                {
                    response = await base.SendAsync(request, cancellationToken).ConfigureAwait(false);
                    response.EnsureSuccessStatusCode();
                    break;
                }
                catch (Exception ex)
                {
                    if (++attempt > MaxRetryCount || !HttpExceptionHandler.IsTransient(ex))
                    {
                        throw;
                    }
                    delayTime += IncrementTime;
                }
                await Task.Delay(delayTime, cancellationToken).ConfigureAwait(false);
            }

            return response;
        }
    }
}