﻿namespace OpenWeatherMap.BL
{
    using System.Net;
    using System.Threading.Tasks;

    using CommandBus.Contracts;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    using OpenWeatherMap.Commands;

    public class OpenWeatherMapCommandHandler : ICommandHandler<GetWeatherByCityIdCommand>, ICommandHandler<GetWeatherByCityCommand>
    {
        private readonly WebClient webClient;

        public OpenWeatherMapCommandHandler()
        {
            this.webClient = new WebClient
                                 {
                                     BaseAddress = "http://api.openweathermap.org/data/2.5/",
                                     UseDefaultCredentials = true
                                 };
        }

        public async Task<CommandResult> Handle(GetWeatherByCityIdCommand command)
        {
            var result = new GetWeatherByCityIdCommandResult();

            var request = "group?id=" + command.CityId + "&units=metric";
            var response = await this.webClient.DownloadStringTaskAsync(request).ConfigureAwait(false);

            var json = (JObject)JsonConvert.DeserializeObject(response);
            var city = json["list"][0];

            result.Name = city.Value<string>("name");
            result.Main = city["weather"][0].Value<string>("main");
            result.Description = city["weather"][0].Value<string>("description");

            return result;
        }

        public async Task<CommandResult> Handle(GetWeatherByCityCommand command)
        {
            var result = new GetWeatherByCityCommandResult();

            var request = "forecast/daily?q=" + command.City + "&units=metric";
            var response = await this.webClient.DownloadStringTaskAsync(request).ConfigureAwait(false);

            var json = (JObject)JsonConvert.DeserializeObject(response);
            
            result.Country = json["city"].Value<string>("country");
            result.Name = json["city"].Value<string>("name");

            var city = json["list"][0];
            result.Main = city["weather"][0].Value<string>("main");
            result.Description = city["weather"][0].Value<string>("description");

            return result;
        }
    }
}