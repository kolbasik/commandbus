﻿namespace OpenWeatherMap.BL
{
    using System.ComponentModel.Design;

    using CommandBus.Contracts;

    using OpenWeatherMap.Commands;

    public static class OpenWeatherMapStartup
    {
        public static void Register(ServiceContainer serviceContainer)
        {
            var openWeatherMapCommandHandler = new OpenWeatherMapCommandHandler();
            serviceContainer.AddService(typeof(ICommandHandler<GetWeatherByCityIdCommand>), openWeatherMapCommandHandler, true);
            serviceContainer.AddService(typeof(ICommandHandler<GetWeatherByCityCommand>), openWeatherMapCommandHandler, true);
        }
    }
}
