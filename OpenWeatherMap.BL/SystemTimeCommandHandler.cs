﻿namespace OpenWeatherMap.BL
{
    using System;
    using System.Threading.Tasks;

    using CommandBus.Contracts;

    using OpenWeatherMap.Commands;

    public class SystemTimeCommandHandler : ICommandHandler<GetSystemTimeCommand>
    {
        public Task<CommandResult> Handle(GetSystemTimeCommand command)
        {
            CommandResult commandResult = new GetSystemTimeCommandResult { SystemTime = DateTime.UtcNow };

            return Task.FromResult(commandResult);
        }
    }
}