namespace CommandBus.ServiceProvider
{
    using System;
    using System.ComponentModel.Design;

    using CommandBus.Core;

    public class ServiceProviderCommandBusBuilder : CommandBusBuilder<ServiceProviderCommandBus>
    {
        private readonly ServiceContainer container;

        public ServiceProviderCommandBusBuilder()
            : this(new ServiceContainer())
        {
        }

        public ServiceProviderCommandBusBuilder(ServiceContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }

        public override ServiceProviderCommandBus BuildCommandBus()
        {
            foreach (var item in this.MetadataList)
            {
                var metadata = item;
                this.container.AddService(metadata.CommandHandlerInterface, (serviceContainer, type) => Activator.CreateInstance(metadata.CommandHandlerType));
            }
            return new ServiceProviderCommandBus(this.container, this.MiddlewareList);
        }
    }
}