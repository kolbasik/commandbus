namespace CommandBus.ServiceProvider
{
    using System;
    using System.Threading.Tasks;

    using CommandBus.Contracts;
    using CommandBus.Core;

    public sealed class ServiceProviderCommandBusPipeline : CommandBusPipeline
    {
        private readonly IServiceProvider container;

        private readonly CommandBusMiddleware middleware;

        public ServiceProviderCommandBusPipeline(IServiceProvider container, params object[] middlewareList)
            : base(middlewareList)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
            this.middleware = new CommandBusMiddlewareCommandHandlerAdapter(container);
        }

        protected override CommandBusMiddleware Resolve(Command command)
        {
            return this.middleware;
        }

        protected override CommandBusMiddleware Resolve(Type middlewareType)
        {
            return (CommandBusMiddleware)this.container.GetService(middlewareType);
        }

        private sealed class CommandBusMiddlewareCommandHandlerAdapter : CommandBusMiddleware
        {
            private readonly IServiceProvider container;

            private readonly CommandMetadataResolver metadataResolver;

            public CommandBusMiddlewareCommandHandlerAdapter(IServiceProvider container)
            {
                this.container = container;
                this.metadataResolver = new CommandMetadataResolver();
            }

            public override async Task Send(CommandBusContext context)
            {
                if (context == null || context.Command == null)
                {
                    throw new ArgumentNullException("context");
                }
                var commandType = context.Command.GetType();
                var metadata = this.metadataResolver.ResolveCommandHandlerMetadata(commandType);
                var commandHandler = this.container.GetService(metadata.CommandHandlerType);
                if (commandHandler == null)
                {
                    throw new CommandHandlerNotFoundException(commandType);
                }
                try
                {
                    var task = (Task<CommandResult>)metadata.CommandHandleMethod.Invoke(commandHandler, new object[] { context.Command });
                    context.CommandResult = await task.ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    context.Exception = ex;
                }
            }
        }
    }
}