﻿namespace CommandBus.ServiceProvider
{
    using System;
    using System.Threading.Tasks;

    using CommandBus.Contracts;
    using CommandBus.Core;

    public sealed class ServiceProviderCommandBus : CommandBus, IGenericCommandBus
    {
        private readonly IServiceProvider container;

        public ServiceProviderCommandBus(IServiceProvider container, params object[] middlewareList)
            : base(new ServiceProviderCommandBusPipeline(container, middlewareList))
        {
            this.container = container;
        }

        public Task<CommandResult> Submit<TCommand>(TCommand command) where TCommand : Command
        {
            var commandHandler = this.container.GetService<ICommandHandler<TCommand>>();
            if (commandHandler == null)
            {
                throw new CommandHandlerNotFoundException(typeof(TCommand));
            }
            return commandHandler.Handle(command);
        }
    }
}