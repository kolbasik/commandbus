namespace CommandBus.Http.Server
{
    using System;
    using System.IO;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web;

    using CommandBus.Contracts;

    using Newtonsoft.Json;

    /// <summary>
    ///     Summary description for CommandBus
    /// </summary>
    public abstract class HttpCommandBusHandler : HttpTaskAsyncHandler
    {
        public abstract ICommandBus CommandBus { get; }

        /// <summary>
        /// When overridden in a derived class, provides code that handles an asynchronous task.
        /// </summary>
        /// <param name="context">The HTTP context.</param>
        /// <returns>
        /// The asynchronous task.
        /// </returns>
        public override async Task ProcessRequestAsync(HttpContext context)
        {
            CommandResult commandResult;
            try
            {
                Command command = GetCommand(context.Request);
                commandResult = await this.CommandBus.Submit(command).ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                commandResult = new FaultCommandResult(ex);
            }

            if (commandResult == null)
            {
                SetHttpStatusCode(context.Response, HttpStatusCode.NoContent);
            }
            else if (commandResult is FaultCommandResult)
            {
                SetHttpStatusCode(context.Response, HttpStatusCode.BadRequest);
                SetCommandResult(context.Response, commandResult);
            }
            else
            {
                SetHttpStatusCode(context.Response, HttpStatusCode.OK);
                SetCommandResult(context.Response, commandResult);
            }
        }

        private static Command GetCommand(HttpRequest request)
        {
            var commandType = Type.GetType(request.Headers[HttpCommandBusConstants.Headers.CommandType]);
            var upstream = new StreamReader(request.InputStream, Encoding.UTF8).ReadToEnd();
            var command = (Command)JsonConvert.DeserializeObject(upstream, commandType);
            return command;
        }

        private static void SetHttpStatusCode(HttpResponse response, HttpStatusCode httpStatusCode)
        {
            response.StatusCode = (int)httpStatusCode;
        }

        private static void SetCommandResult(HttpResponse response, CommandResult commandResult)
        {
            var commandResultType = commandResult.GetType();
            var downstream = JsonConvert.SerializeObject(commandResult);
            response.Headers.Add(HttpCommandBusConstants.Headers.CommandResultType, commandResultType.AssemblyQualifiedName);
            response.ContentType = @"application/json";
            response.Write(downstream);
        }
    }
}