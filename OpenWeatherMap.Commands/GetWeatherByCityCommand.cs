namespace OpenWeatherMap.Commands
{
    using CommandBus.Contracts;

    public class GetWeatherByCityCommand : CommandFor<GetWeatherByCityCommandResult>
    {
        public string City { get; set; }
    }
}