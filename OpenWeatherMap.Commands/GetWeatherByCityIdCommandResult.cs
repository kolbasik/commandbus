namespace OpenWeatherMap.Commands
{
    using System.Globalization;

    using CommandBus.Contracts;

    public class GetWeatherByCityIdCommandResult : CommandResult
    {
        public string Name { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.InvariantCulture,
                "City: {0}; Main: {1}; Description: {2}", this.Name, this.Main, this.Description);
        }
    }
}