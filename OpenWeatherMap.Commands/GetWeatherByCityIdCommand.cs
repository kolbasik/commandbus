﻿namespace OpenWeatherMap.Commands
{
    using CommandBus.Contracts;

    public class GetWeatherByCityIdCommand : CommandFor<GetWeatherByCityIdCommandResult>
    {
        public string CityId { get; set; }
    }
}