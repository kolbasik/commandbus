﻿namespace OpenWeatherMap.Commands
{
    using System.Globalization;

    using CommandBus.Contracts;

    public class GetWeatherByCityCommandResult : CommandResult
    {
        public string Country { get; set; }
        public string Name { get; set; }

        public string Main { get; set; }
        public string Description { get; set; }

        public override string ToString()
        {
            return string.Format(
                CultureInfo.InvariantCulture,
                "Country: {0}; City: {1}; Main: {2}; Description: {3}", this.Country, this.Name, this.Main, this.Description);
        }
    }
}