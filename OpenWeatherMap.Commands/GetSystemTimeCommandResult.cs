﻿namespace OpenWeatherMap.Commands
{
    using System;
    using System.Globalization;

    using CommandBus.Contracts;

    public class GetSystemTimeCommandResult : CommandResult
    {
        public DateTime SystemTime { get; set; }
        
        public override string ToString()
        {
            return string.Format(CultureInfo.InvariantCulture, "System Time: {0}", this.SystemTime);
        }
    }
}