namespace CommandBus.Contracts
{
    public abstract class CommandFor<TCommandResult> : Command
        where TCommandResult : CommandResult
    {
    }
}