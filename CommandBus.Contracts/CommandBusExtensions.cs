namespace CommandBus.Contracts
{
    using System.Threading.Tasks;

    public static class CommandBusExtensions
    {
        public static async Task<TCommandResult> Submit<TCommandResult>(this ICommandBus commandBus, CommandFor<TCommandResult> command)
            where TCommandResult : CommandResult
        {
            var commandResult = await commandBus.Submit(command).ConfigureAwait(false);
            return (TCommandResult)commandResult;
        }

        public static async Task<TCommandResult> Submit<TCommand, TCommandResult>(this ICommandBus commandBus, TCommand command)
            where TCommand : CommandFor<TCommandResult>
            where TCommandResult : CommandResult
        {
            CommandResult commandResult;
            var nativeCommandBus = commandBus as IGenericCommandBus;
            if (nativeCommandBus != null)
            {
                commandResult = await nativeCommandBus.Submit<TCommand>(command).ConfigureAwait(false);
            }
            else
            {
                commandResult = await commandBus.Submit(command).ConfigureAwait(false);
            }
            return (TCommandResult)commandResult;
        }
    }
}