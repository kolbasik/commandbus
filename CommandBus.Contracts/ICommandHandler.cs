namespace CommandBus.Contracts
{
    using System.Threading.Tasks;

    public interface ICommandHandler<in TCommand> where TCommand : Command
    {
        Task<CommandResult> Handle(TCommand command);
    }
}