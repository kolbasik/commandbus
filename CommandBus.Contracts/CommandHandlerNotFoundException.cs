﻿namespace CommandBus.Contracts
{
    using System;

    [Serializable]
    public sealed class CommandHandlerNotFoundException : Exception
    {
        public CommandHandlerNotFoundException(Type commandHandlerType)
            : base(string.Format("Cannot find a command handler for '{0}'", commandHandlerType.FullName))
        {
        }

        public CommandHandlerNotFoundException(string message)
            : base (message)
        {
        }
    }
}