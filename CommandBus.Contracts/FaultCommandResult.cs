﻿namespace CommandBus.Contracts
{
    using System;

    public class FaultCommandResult : CommandResult
    {
        public FaultCommandResult()
        {
        }

        public FaultCommandResult(Exception exception)
        {
            this.Message = exception.Message;
            this.Details = exception.ToString();
        }

        public string Message { get; set; }

        public string Details { get; set; }
    }

    public class FaultCommandResult<TException> : FaultCommandResult
        where TException : Exception
    {
        public FaultCommandResult()
        {
        }

        public FaultCommandResult(TException exception)
            : base(exception)
        {
            this.Fault = exception;
        }

        public TException Fault { get; set; }
    }
}