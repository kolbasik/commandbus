namespace CommandBus.Contracts
{
    using System.Threading.Tasks;

    public interface ICommandBus
    {
        Task<CommandResult> Submit(Command command);
    }
}