namespace CommandBus.Contracts
{
    using System;

    [Serializable]
    public sealed class FaultCommandResultException : Exception
    {
        public FaultCommandResultException(string message, Exception innerException = null)
            : base(message, innerException)
        {
        }

        public FaultCommandResultException(string message, string details)
            : base(message)
        {
            this.Details = details;
        }

        public string Details { get; private set; }
    }
}