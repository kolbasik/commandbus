namespace CommandBus.Contracts
{
    using System.Threading.Tasks;

    public interface IGenericCommandBus
    {
        Task<CommandResult> Submit<TCommand>(TCommand command) where TCommand : Command;
    }
}