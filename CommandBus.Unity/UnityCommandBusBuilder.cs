namespace CommandBus.Unity
{
    using System;
    using System.Threading.Tasks;

    using CommandBus.Contracts;
    using CommandBus.Core;

    using Microsoft.Practices.Unity;

    public class UnityCommandBusBuilder : CommandBusBuilder<UnityCommandBus>
    {
        private readonly IUnityContainer container;

        public UnityCommandBusBuilder()
            : this(new UnityContainer())
        {
        }

        public UnityCommandBusBuilder(IUnityContainer container)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container.CreateChildContainer();
        }

        public override UnityCommandBus BuildCommandBus()
        {
            foreach (var metadata in this.MetadataList)
            {
                var commandHandlerMiddlewareType = typeof(CommandBusMiddlewareCommandHandlerAdapter<,>).MakeGenericType(metadata.CommandHandlerType, metadata.CommandType);
                this.container.RegisterType(typeof(CommandBusMiddleware), commandHandlerMiddlewareType, metadata.CommandType.FullName);
                this.container.RegisterType(metadata.CommandHandlerInterface, metadata.CommandHandlerType);
            }
            return new UnityCommandBus(this.container, this.MiddlewareList);
        }

        private sealed class CommandBusMiddlewareCommandHandlerAdapter<TCommandHandler, TCommand> : CommandBusMiddleware
            where TCommandHandler : ICommandHandler<TCommand>
            where TCommand : Command
        {
            private readonly TCommandHandler commandHandler;

            public CommandBusMiddlewareCommandHandlerAdapter(TCommandHandler commandHandler)
            {
                if (commandHandler == null)
                {
                    throw new ArgumentNullException("commandHandler");
                }
                this.commandHandler = commandHandler;
            }

            public override async Task Send(CommandBusContext context)
            {
                if (context == null || context.Command == null)
                {
                    throw new ArgumentNullException("context");
                }
                var command = context.Command as TCommand;
                if (command == null)
                {
                    var message = string.Format("Invalid UnityCommandBus configuration. '{0}' cannot handle {1}.",
                        typeof(TCommandHandler).FullName,
                        context.Command.GetType().FullName);
                    throw new NotSupportedException(message);
                }
                try
                {
                    context.CommandResult = await this.commandHandler.Handle(command).ConfigureAwait(false);
                }
                catch (Exception ex)
                {
                    context.Exception = ex;
                }
            }
        }
    }
}