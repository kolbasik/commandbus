﻿namespace CommandBus.Unity
{
    using System.Threading.Tasks;

    using CommandBus.Contracts;

    using Microsoft.Practices.Unity;

    public class UnityCommandBus : Core.CommandBus, IGenericCommandBus
    {
        private readonly IUnityContainer container;

        public UnityCommandBus(IUnityContainer container, params object[] middlewareList)
            : base(new UnityCommandBusPipeline(container, middlewareList))
        {
            this.container = container;
        }

        public Task<CommandResult> Submit<TCommand>(TCommand command) where TCommand : Command
        {
            var commandHandler = this.container.Resolve<ICommandHandler<TCommand>>();
            if (commandHandler == null)
            {
                throw new CommandHandlerNotFoundException(typeof(TCommand));
            }
            return commandHandler.Handle(command);
        }
    }
}