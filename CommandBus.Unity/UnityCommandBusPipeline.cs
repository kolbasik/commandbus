namespace CommandBus.Unity
{
    using System;

    using CommandBus.Contracts;
    using CommandBus.Core;

    using Microsoft.Practices.Unity;

    public sealed class UnityCommandBusPipeline : CommandBusPipeline
    {
        private readonly IUnityContainer container;

        public UnityCommandBusPipeline(IUnityContainer container, params object[] middlewareList)
            : base(middlewareList)
        {
            if (container == null)
            {
                throw new ArgumentNullException("container");
            }
            this.container = container;
        }

        protected override CommandBusMiddleware Resolve(Command command)
        {
            return this.container.Resolve<CommandBusMiddleware>(command.GetType().FullName);
        }

        protected override CommandBusMiddleware Resolve(Type middlewareType)
        {
            return (CommandBusMiddleware)this.container.Resolve(middlewareType);
        }
    }
}