namespace ServiceBusPoC.ConsoleApp
{
    using System;
    using System.ComponentModel.Design;
    using System.Threading.Tasks;

    using CommandBus.Contracts;
    using CommandBus.Http;
    using CommandBus.ServiceProvider;

    using OpenWeatherMap.BL;
    using OpenWeatherMap.Commands;

    internal class WeatherDemo
    {
        public static async Task Start()
        {
            Console.WriteLine("HostCommandBusDemo:");
            await HostCommandBusDemo().ConfigureAwait(false);

            Console.WriteLine("HttpCommandBusDemo:");
            await HttpCommandBusDemo().ConfigureAwait(false);
        }

        private static async Task HostCommandBusDemo()
        {
            var serviceContainer = new ServiceContainer();
            OpenWeatherMapStartup.Register(serviceContainer);
            var commandBus = new ServiceProviderCommandBus(serviceContainer);

            await GetWeather(commandBus);
        }

        private static async Task HttpCommandBusDemo()
        {
            var commandBus = new HttpCommandBus(new Uri("http://localhost:56988/ServiceProviderCommandBusProxy.ashx"));

            await GetWeather(commandBus);
        }

        private static async Task GetWeather(ICommandBus commandBus)
        {
            var cityIds = new[] { "524901", "703448"/*, "2643743"*/ };
            foreach (var cityId in cityIds)
            {
                var command = new GetWeatherByCityIdCommand { CityId = cityId };
                var commandResult = await commandBus.Submit<GetWeatherByCityIdCommandResult>(command).ConfigureAwait(false);

                Console.WriteLine(commandResult);
            }

            var cityNames = new[] { "Kyiv,UA", "Odessa,UA" };
            foreach (var cityName in cityNames)
            {
                var command = new GetWeatherByCityCommand { City = cityName };
                var commandResult = await commandBus.Submit<GetWeatherByCityCommand, GetWeatherByCityCommandResult>(command).ConfigureAwait(false);

                Console.WriteLine(commandResult);
            }
        }
    }
}