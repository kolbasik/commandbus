namespace ServiceBusPoC.ConsoleApp
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;

    using CommandBus.Contracts;
    using CommandBus.Http;
    using CommandBus.ServiceProvider;
    using CommandBus.Unity;

    using OpenWeatherMap.BL;
    using OpenWeatherMap.Commands;

    internal class PerformanceDemo
    {
        private const int Count = 2000;

        public static async Task Start()
        {
            for (int i = 0; i < 10; i++)
            {
                await Attempt().ConfigureAwait(false);
            }
        }

        private static async Task Attempt()
        {
            var tuples = new List<Tuple<string, Func<Task<TimeSpan>>>>()
                             {
                                 Tuple.Create<string, Func<Task<TimeSpan>>>("ServiceProvider:", ServiceProviderCommandBusDemo),
                                 Tuple.Create<string, Func<Task<TimeSpan>>>("Unity:", UnityCommandBusDemo),
                                 Tuple.Create<string, Func<Task<TimeSpan>>>("ServiceProvider Http:", ServiceProviderHttpCommandBusDemo),
                                 Tuple.Create<string, Func<Task<TimeSpan>>>("Unity Http:", UnityHttpCommandBusDemo),
                             };
            var ticks = new List<long>();
            Console.WriteLine("****");
            foreach (var tuple in tuples)
            {
                Console.Write(tuple.Item1.PadRight(25));
                var time = await tuple.Item2().ConfigureAwait(false);
                Console.WriteLine("Total: {0}, Count/Sec: {1}", time, (int)Math.Ceiling(Count * 1.0 / time.TotalSeconds));
                ticks.Add(time.Ticks);
            }
            Console.WriteLine("------");
            var min = ticks.Min();
            Console.WriteLine("x: {0}", string.Join(", ", ticks.Select(x => x / min)));
        }

        private static Task<TimeSpan> ServiceProviderCommandBusDemo()
        {
            var serviceProviderCommandBusBuilder = new ServiceProviderCommandBusBuilder();
            serviceProviderCommandBusBuilder.RegisterCommandHandlersFrom(typeof(OpenWeatherMapStartup).Assembly);
            var commandBus = serviceProviderCommandBusBuilder.BuildCommandBus();

            return MeasureGetSystemTime(commandBus);
        }

        private static Task<TimeSpan> UnityCommandBusDemo()
        {
            var unityCommandBusBuilder = new UnityCommandBusBuilder();
            unityCommandBusBuilder.RegisterCommandHandlersFrom(typeof(OpenWeatherMapStartup).Assembly);
            var commandBus = unityCommandBusBuilder.BuildCommandBus();

            return MeasureGetSystemTime(commandBus);
        }

        private static Task<TimeSpan> ServiceProviderHttpCommandBusDemo()
        {
            var commandBus = new HttpCommandBus(new Uri("http://localhost:56988/ServiceProviderCommandBusProxy.ashx"));

            return MeasureGetSystemTime(commandBus);
        }

        private static Task<TimeSpan> UnityHttpCommandBusDemo()
        {
            var commandBus = new HttpCommandBus(new Uri("http://localhost:56988/UnityCommandBusProxy.ashx"));

            return MeasureGetSystemTime(commandBus);
        }

        private static async Task<TimeSpan> MeasureGetSystemTime(ICommandBus commandBus)
        {
            var stopwatch = Stopwatch.StartNew();

            var tasks = new List<Task>();
            for (int i = 0; i < Count; i++)
            {
                tasks.Add(GetSystemTime(commandBus));
            }
            await Task.WhenAll(tasks).ConfigureAwait(false);

            stopwatch.Stop();
            return stopwatch.Elapsed;
        }

        private static async Task GetSystemTime(ICommandBus commandBus)
        {
            var command = new GetSystemTimeCommand();
            var commandResult = await commandBus.Submit<GetSystemTimeCommandResult>(command).ConfigureAwait(false);
            Debug.Assert(commandResult != null);
        }
    }
}