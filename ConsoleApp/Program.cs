﻿namespace ServiceBusPoC.ConsoleApp
{
    using System;

    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Press any key to start...");
            Console.ReadKey(false);

            WeatherDemo.Start().Wait();
            PerformanceDemo.Start().Wait();

            Console.WriteLine("Press any key to stop...");
            Console.ReadKey(false);
        }
    }
}